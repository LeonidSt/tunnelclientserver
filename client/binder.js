'use strict';


var ACTION_TYPE = {
    'CALL': 0,
    'RESULT': 1,
    'CREATE_CONTEXT': 2,
    'ANNOUNCE': 3,

    'MAX_TYPE': 4
};


var id = 0;
function get_id() {
    ++id;
    return id;
}


class Client {

    constructor(url, ready) {
        console.log(`connecting: ${url}`);

        this.connection = new WebSocket(url);
        this.connection.onopen = ready;

        this.waiters = {};
        this.contexts = {};
        this.connection.onmessage = this._on_message.bind(this);
    }

    create_context(on_created) {
        var message = {
            'id': get_id(),
            'context_id': 0,
            'action': ACTION_TYPE['CREATE_CONTEXT'],
            'name': '',
            'body': ''
        };

        this._send(message, response => {
            this.contexts[response.context_id] = new Context(response.context_id, this);
            on_created(this.contexts[response.context_id]);
        });
    }

    _on_message(event) {
        var message = JSON.parse(event.data);

        if (message.action == ACTION_TYPE['ANNOUNCE'] && message.context_id in this.contexts) {
            this.contexts[message.context_id].announce(message.body);
            return;
        }

        if (!(message.id in this.waiters))
            return;

        this.waiters[message.id](message);
        delete this.waiters[message.id];
    }

    _send(message, callback) {
        this.connection.send(JSON.stringify(message));

        if (callback != null)
            this.waiters[message.id] = callback;
    }

    close() {
        this.connection.close();
        console.log(`connection close`);
    }
}


class Context {

    constructor(id, client) {
        this.id = id;
        this.binded = [];
        this.client = client;

        console.log(`context created: ${id}`);
    }


    announce(methods) {
        var methods_list = methods.split(",");

        for (let i = 0; i < methods_list.length; ++i) {

            let a = function (context) {
                return function () {
                    let body = Array.prototype.slice.call(arguments).join(',,');
                    let message = {
                        'id': get_id(),
                        'context_id': context.id,
                        'action': ACTION_TYPE['CALL'],
                        'name': methods_list[i],
                        'body': body
                    };

                    let promise = new Promise((resolve) => {
                        context.client._send(message, result => {
                            resolve(result.body);
                        });
                    });

                    return promise;
                }
            }(this);

            this.binded[methods_list[i]] = a;
        }

        if (this.on_announce !== undefined)
            this.on_announce();
    }
}
