#include "web_tunnel/server.h"
#include "web_tunnel_interface/web_tunnel.h"

#include <functional>

using namespace std;

int baz(int a, int b)
{
    return a + b;
}

int antibaz(int a, int b)
{
    return -(a + b);
}

std::string id(std::string s)
{
    return s;
}

void simple_server_test()
{
    using namespace web_tunnel;
    vector<shared_ptr<web_context>> contexts;

    web_server server(8080, [&contexts](auto context)
    {
        // on create context
        contexts.push_back(context);
        web_tunnel::bind_function("baz", baz, context);
        web_tunnel::announce(context);
    });
}

void test_multple_context_binding()
{
    using namespace web_tunnel;
    vector<shared_ptr<web_context>> contexts;

    int i = 0;
    web_server server(8080, [&contexts, &i](auto context)
    {
        contexts.push_back(context);

        if (++i % 2 == 0)
            web_tunnel::bind_function("baz", baz, context);
        else
            web_tunnel::bind_function("baz", antibaz, context);


        std::function<int(int)> hello = [i](int j)
        {
            return j * i;
        };

        web_tunnel::bind_function("hello", hello, context);
        web_tunnel::announce(context);
    });
}

void test_round_trip()
{
    using namespace web_tunnel;
    vector<shared_ptr<web_context>> contexts;

    web_server server(8080, [&contexts](auto context)
    {
        // on create context
        contexts.push_back(context);
        web_tunnel::bind_function("id", id, context);
        web_tunnel::announce(context);
    }, false);
}

int main()
{
//    simple_server_test();
//    test_multple_context_binding();
    test_round_trip();
}
