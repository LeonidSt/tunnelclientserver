#include "protocol.h"

namespace web_tunnel
{

namespace details
{

using namespace rapidjson;

void header::read(Document &json)
{
    message_id = json["id"].GetUint();
    action = static_cast<header::Action>(json["action"].GetUint());
    context_id = json["context_id"].GetUint();
    name = json["name"].GetString();
}

template <class W>
void header::write(W &json) const
{
    json.String("id");
    json.Uint(message_id);

    json.String("action");
    json.Uint(action);

    json.String("context_id");
    json.Uint(context_id);

    json.String("name");
    json.String(name.c_str());
}

message::message()
{}

message::message(string const& str)
{
    Document json;
    json.Parse(str.c_str());

    head.read(json);
    body = json["body"].GetString();
}

string message::to_string() const
{
    StringBuffer buffer;

    PrettyWriter<StringBuffer> writer(buffer);
    writer.StartObject();

    head.write<PrettyWriter<StringBuffer>>(writer);

    writer.String("body");
    writer.String(body.c_str());
    writer.EndObject();

    return string(buffer.GetString());
}

message create_response(message const& other)
{
    message result;
    result.head = other.head;
    result.head.action = header::RESULT;

    return result;
}

}

}
