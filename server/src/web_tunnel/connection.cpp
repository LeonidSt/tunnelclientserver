#include "connection.h"

#include "protocol.h"

namespace web_tunnel
{

connection::connection(server_t &server, connection_hdl handler, callback_t create_callback)
    : server_(server)
    , connection_(handler)
    , create_callback_(create_callback)
{
    auto link = server_.get_con_from_hdl(connection_);
    link->set_message_handler(bind(&connection::on_message, this, _1, _2));
}

connection::~connection()
{}

void connection::on_message(connection_hdl handler, server_t::message_ptr client_message)
{
    message msg(client_message->get_payload());

    if (msg.head.action == header::CREATE_CONTEXT)
    {
        create_context(msg);
        return;
    }

    auto context = contexts_.find(msg.head.context_id);
    if (context == contexts_.end())
        return;

    context->second->on_message(msg);
}

void connection::send_message(string message)
{
    server_.send(connection_, message, websocketpp::frame::opcode::text);
}

void connection::create_context(message& msg)
{
    static int i = 1;
    auto context = std::make_shared<web_context>(this, i);
    contexts_.insert({i, context});

    message response = create_response(msg);
    response.head.action = header::CREATE_CONTEXT;
    response.head.context_id = i;


    ++i;
    send_message(response.to_string());

    create_callback_(context);
}

}

