#pragma once

#include "web_context.h"
#include "invoke.h"
#include "protocol.h"

#include <websocketpp/server.hpp>
#include <websocketpp/common/thread.hpp>
#include <websocketpp/config/asio_no_tls.hpp>


namespace web_tunnel
{

typedef websocketpp::server<websocketpp::config::asio> server_t;

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

struct connection
{
    typedef std::function<void(shared_ptr<web_context>)> callback_t;

    connection(server_t &server, connection_hdl handler, callback_t context_create);
    virtual ~connection();

    void on_message(connection_hdl handler, server_t::message_ptr client_message);
    void send_message(string message);
    void create_context(message &msg);

private:
    server_t &server_;
    connection_hdl connection_;
    callback_t create_callback_;

    std::unordered_map<uint32_t, shared_ptr<web_context>> contexts_;
};

}
