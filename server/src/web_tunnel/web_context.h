#pragma once

#include "invoke.h"
#include "protocol.h"

#include <websocketpp/server.hpp>
#include <websocketpp/common/thread.hpp>
#include <websocketpp/config/asio_no_tls.hpp>


namespace web_tunnel
{

typedef websocketpp::server<websocketpp::config::asio> server_t;

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

using namespace details;

struct connection;

struct web_context
{

    web_context(connection *con, int id);
    virtual ~web_context();

    template <class Functor>
    void bind_function(string const& name, Functor functor)
    {
        binded_[name] = [functor](string args, callback_t callback)
        {
            callback(convert::to_string(invoke(functor, args)));
        };
    }

    void on_message(details::message &msg);
    void call(message const& msg);
    void announce() const;

    void is_web_context() const;

private:

    typedef std::function<void(string const&)> callback_t;

    int id_;
    connection *connection_;
    std::unordered_map<string, std::function<void(string const&, callback_t)>> binded_;
};

}
