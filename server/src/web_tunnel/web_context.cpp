#include "web_context.h"

#include "connection.h"


namespace web_tunnel
{

web_context::web_context(connection *con, int id)
    : connection_(con)
    , id_(id)
{}

web_context::~web_context()
{}

void web_context::on_message(message &msg)
{
    switch (msg.head.action)
    {
        case header::CALL:
            call(msg);
            break;
        case header::RESULT:
            break;
        case header::CREATE_CONTEXT:
        case header::ANNOUNCE:
        case header::MAX_TYPE:
            break;
    }
}

void web_context::call(message const& msg)
{
    auto func = binded_.find(msg.head.name);
    if (func == binded_.end())
    {
        std::cout << "Call: function not found(" << msg.head.name << ")" << std::endl;
        return;
    }

    func->second(msg.body, [msg, this](string result) {
        message response = create_response(msg);
        response.body = result;

        connection_->send_message(response.to_string());
    });
}

void web_context::announce() const
{
    message response;
    response.head.action = header::ANNOUNCE;
    response.head.context_id = id_;

    std::stringstream msg;

    for (auto &i : binded_)
    {
        if (msg.tellp())
        {
            msg << ",";
        }
        msg << i.first;
    }

    response.body = msg.str();

    connection_->send_message(response.to_string());
}

}
