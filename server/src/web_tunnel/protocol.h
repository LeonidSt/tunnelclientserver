#pragma once

#include <cstdint>

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/stringbuffer.h"

namespace web_tunnel
{

namespace details
{

struct header
{
    enum Action : uint8_t
    {
        CALL            = 0,
        RESULT          = 1,
        CREATE_CONTEXT  = 2,
        ANNOUNCE        = 3,

        MAX_TYPE
    };

    void read(rapidjson::Document &json);

    template <class Writer>
    void write(Writer &writer) const;

    uint32_t message_id = 0;
    uint32_t context_id = 0;

    Action action;
    string name;
};

struct message
{
    header head;
    string body;

    message();
    message(string const& src);

    string to_string() const;
};

message create_response(message const& other);

} // details

} // web_tunnel

