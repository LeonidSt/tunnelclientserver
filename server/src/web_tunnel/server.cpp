#include "server.h"

namespace web_tunnel
{

web_server::web_server(uint16_t port, std::function<void(std::shared_ptr<web_context>)> cb, bool logging)
    : create_cb_(cb)
    , server_thread_(std::bind(&web_server::run, this, logging))
{}

void web_server::run(bool logging)
{
    server_.set_open_handler(std::bind(&web_server::on_open, this, std::placeholders::_1));

    server_.init_asio();
    server_.listen(8080);
    server_.start_accept();

    if (!logging) {
        server_.clear_access_channels(websocketpp::log::alevel::all);
    }

    server_.run();
}

web_server::~web_server()
{
    server_thread_.join();
}

void web_server::on_open(connection_hdl handler)
{
    connections_.push_back(std::make_shared<connection>(server_, handler, create_cb_));
}

}