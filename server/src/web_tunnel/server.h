#pragma once

#include "connection.h"
#include "web_context.h"

#include <cstdint>
#include <functional>
#include <string>
#include <unordered_map>
#include <thread>

#include <websocketpp/server.hpp>
#include <websocketpp/common/thread.hpp>
#include <websocketpp/config/asio_no_tls.hpp>

namespace web_tunnel
{

typedef websocketpp::server<websocketpp::config::asio> server_t;

using websocketpp::connection_hdl;
using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;


struct web_server
{
    typedef std::function<void(shared_ptr<web_context>)> callback_t;

    web_server(uint16_t port, callback_t on_context_create, bool logging = true);
    void run(bool logging);
    virtual ~web_server();

    void on_open(connection_hdl handler);

private:
    callback_t create_cb_;
    std::thread server_thread_;
    server_t server_;

    vector<shared_ptr<connection>> connections_;
};



}


