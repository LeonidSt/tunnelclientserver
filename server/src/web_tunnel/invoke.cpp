#include "invoke.h"

#include <cassert>

namespace convert
{

std::istream& operator>>(std::istream& stream, vector<int>& result)
{
    int i;
    char c;

    while (stream >> i)
    {
        result.push_back(i);
        stream >> c;
    }

    return stream;
}

}
