#pragma once

namespace convert
{

std::istream& operator>>(std::istream& stream, vector<int>& result);

template <class T>
T from_string(string something)
{
    std::stringstream s(something);
    T t{};
    s >> t;
    return t;
}

template <class T>
string to_string(T t)
{
    std::stringstream s;
    s << t;
    return s.str();
}

/*
    template <>
    dict_t from_string<dict_t>(std::string something)
    {
        std::stringstream s(something);
        return json_io::stream_to_dict(s);
    }

    template <>
    std::string to_string<dict_t>(dict_t dict)
    {
        std::stringstream out;
        json_io::dict_to_stream(out, dict, true);
        return out.str();
    }
*/

}

namespace web_tunnel
{

namespace details
{
template <class F, class T, size_t... index>
decltype(auto) apply_impl(F func, T args, std::index_sequence<index...>)
{
    return func(std::get<index>(std::forward<T>(args))...);
};

template <class F, class T>
decltype(auto) apply(F func, T&& args)
{
    return apply_impl(func, std::forward<T>(args), std::make_index_sequence<std::tuple_size<T>::value>());
};

template <class HEAD>
std::tuple <HEAD> parse(std::string arg)
{
    return std::make_tuple(convert::from_string<HEAD>(arg));
}

template <class HEAD, class NECK, class... TAIL>
std::tuple<HEAD, NECK, TAIL...> parse(string arg)
{
    size_t comma = arg.find(",,");
    string head = arg.substr(0, comma);
    string tail = arg.substr(comma + 2);

    return std::tuple_cat(std::make_tuple(convert::from_string<HEAD>(head)), parse<NECK, TAIL...>(tail));
}

}

template <class R, class... Args>
decltype(auto) invoke(std::function<R(Args...)> f, string args)
{
    return details::apply(f, details::parse<Args...>(args));
}

template <class R, class... Args>
decltype(auto) invoke(R(*f)(Args...), string args)
{
    return details::apply(f, details::parse<Args...>(args));
}

}