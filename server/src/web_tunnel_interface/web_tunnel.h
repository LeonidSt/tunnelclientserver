#pragma once

#include <type_traits>

namespace web_tunnel
{

namespace details
{

struct is_web_context
{
    static void yes();

    template <class U>
    static auto check(U* u)
            -> decltype((*u)->is_web_context(), yes());

};

}

template <class F, class C>
auto bind_function(string const& name, F&& function, C context)
    -> decltype(details::is_web_context::check<C>(0))
{
    context->template bind_function<std::decay_t<F>>(name, std::forward<F>(function));
}

template <class C>
auto announce(C context)
    -> decltype(details::is_web_context::check<C>(0))
{
    context->announce();
}

template <class T, class F, class C>
void bind_method(string const& name, F&& method, T* obj, C context);

}
