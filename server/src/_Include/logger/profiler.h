/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once 
#include "common/time_counter.h"

namespace logging
{
    struct item;

    struct LOGGER_API timer
    {
        timer(string const& name);
        timer(string const& name, string const& file, size_t line);

        ~timer();

    private:
        static timer*& cur_profiler();
        void add_child(item&& it);

    private:
        string              name_;
        string              place_;
        timer*              parent_;
        time_counter        counter_;
        std::vector<item>   children_;
    };
}

#define ProfileScope(name) logging::timer t(name, __FILE__, __LINE__)
