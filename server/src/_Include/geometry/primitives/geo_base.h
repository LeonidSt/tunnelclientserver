/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once

#include "geo_base_fwd.h"

namespace geom
{
    struct geo_base_2 : geo_point_2
    {
        geo_base_2 () {}
        geo_base_2 ( geo_point_2 const& base ) : geo_point_2(base) {}

        geo_point_2 operator () ( const point_2  & pos ) const 
        { 
            using namespace merkator_wgs84;
            double factor = wgs84::Rbig*cos(grad2rad(lat));

            // Y coordinate in merkator projection
            double y_base = merkator::melat(grad2rad(lat), wgs84::E)*factor;

            return geo_point_2( rad2grad(merkator::latit((y_base + pos.y)/factor, wgs84::E, 0.001/factor)), 
                                geom::norm180(lon + rad2grad(pos.x/factor)) ) ;
        }
        point_2 operator () ( const geo_point_2  & pos ) const 
        { 
            using namespace merkator_wgs84;
            double factor = wgs84::Rbig*cos(grad2rad(lat));

            // Y coordinate in merkator projection
            double y_base = merkator::melat(grad2rad(lat), wgs84::E)*factor;

            return point_2(grad2rad(pos.lon - lon)*factor, 
                            merkator::melat(grad2rad(pos.lat), wgs84::E)*factor - y_base ) ;
        }

//         geo_point_2 operator () ( const point_2f   & pos ) const { return geo_point_2 ( pos.x + X, pos.y + Y ) ; }
//         geo_point_2 operator () ( const point_2    & pos ) const { return geo_point_2 ( pos.x + X, pos.y + Y ) ; }
//         point_2     operator () ( const geo_point_2& pos ) const { return     point_2 ( pos.X - X, pos.Y - Y ) ; }
// 
//         geo_point_3 operator () ( const point_3f   & pos ) const { return geo_point_3 ( pos.x + X, pos.y + Y, pos.z ) ; }
//         geo_point_3 operator () ( const point_3    & pos ) const { return geo_point_3 ( pos.x + X, pos.y + Y, pos.z ) ; }
//         point_3     operator () ( const geo_point_3& pos ) const { return     point_3 ( pos.X - X, pos.Y - Y, pos.Z ) ; }
// 
//         geo_rect_2  operator () ( const rectangle_2f& rect ) const { return geo_rect_2 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         geo_rect_2  operator () ( const rectangle_2 & rect ) const { return geo_rect_2 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         rectangle_2 operator () ( const geo_rect_2  & rect ) const { return rectangle_2( (*this)(rect.beg),  (*this)(rect.end) ) ; }
// 
//         geo_rect_3  operator () ( const rectangle_3f& rect ) const { return geo_rect_3 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         geo_rect_3  operator () ( const rectangle_3 & rect ) const { return geo_rect_3 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         rectangle_3 operator () ( const geo_rect_3  & rect ) const { return rectangle_3( (*this)(rect.beg),  (*this)(rect.end) ) ; }
// 
         geo_base_2& operator += ( point_2 const pos ) 
         { 
             return *this = geo_base_2((*this)(pos));
         }
    } ;

    struct geo_base_3 : geo_point_3
    {
        geo_base_3 () {}
        geo_base_3 ( geo_point_3 const& base ) : geo_point_3(base) {}
        geo_base_3 ( geo_base_2 const& base, double height ) : geo_point_3(base, height) {}


        geo_point_3 operator () ( const point_3    & pos ) const 
        { 
            return geo_point_3(geo_base_2(*this)(pos), height + pos.z);
        }
        point_3 operator () ( const geo_point_3    & pos ) const 
        { 
            return point_3(geo_base_2(*this)(pos), pos.height - height);
        }
        geo_point_2 operator () ( const point_2    & pos ) const 
        { 
            return geo_point_2(geo_base_2(*this)(pos));
        }
        point_2 operator () ( const geo_point_2    & pos ) const 
        { 
            return point_2(geo_base_2(*this)(pos));
        }


//         geo_point_2 operator () ( const point_2f   & pos ) const { return geo_point_2 ( pos.x + X, pos.y + Y ) ; }
//         geo_point_2 operator () ( const point_2    & pos ) const { return geo_point_2 ( pos.x + X, pos.y + Y ) ; }
//         point_2     operator () ( const geo_point_2& pos ) const { return     point_2 ( pos.X - X, pos.Y - Y ) ; }
// 
//         geo_point_3 operator () ( const point_3f   & pos ) const { return geo_point_3 ( pos.x + X, pos.y + Y, pos.z + Z); }
//         geo_point_3 operator () ( const point_3    & pos ) const { return geo_point_3 ( pos.x + X, pos.y + Y, pos.z + Z); }
//         point_3     operator () ( const geo_point_3& pos ) const { return     point_3 ( pos.X - X, pos.Y - Y, pos.Z - Z); }
// 
//         geo_rect_2  operator () ( const rectangle_2f& rect ) const { return geo_rect_2 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         geo_rect_2  operator () ( const rectangle_2 & rect ) const { return geo_rect_2 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         rectangle_2 operator () ( const geo_rect_2  & rect ) const { return rectangle_2( (*this)(rect.beg),  (*this)(rect.end) ) ; }
// 
//         geo_rect_3  operator () ( const rectangle_3f& rect ) const { return geo_rect_3 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         geo_rect_3  operator () ( const rectangle_3 & rect ) const { return geo_rect_3 ( (*this)(rect.lo()), (*this)(rect.hi()) ) ; }
//         rectangle_3 operator () ( const geo_rect_3  & rect ) const { return rectangle_3( (*this)(rect.beg),  (*this)(rect.end) ) ; }
// 
        geo_base_3& operator += ( point_3 const pos ) 
        { 
            return *this = geo_base_3((*this)(pos));
        }
    } ;

}