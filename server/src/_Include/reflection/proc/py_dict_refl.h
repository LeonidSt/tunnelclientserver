/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once 
#include "serialization/py_dict_ops.h"

namespace script
{
    namespace type_traits
    {
        template<class T> using is_container = binary::type_traits::is_container<T>;
        template<class T> using is_leaf_type = dict::type_traits::is_leaf_type<T>;
        template<class T> using if_leaf_type = dict::type_traits::if_leaf_type<T>;

        template<class T>
        using if_not_leaf_n_container = typename std::enable_if<!is_container<T>::value && !is_leaf_type<T>::value>::type;
    
    } // type_traits

    namespace details
    {
        // processors 
        struct write_processor
        {
            explicit write_processor(py::dict& dict)
                : bd_(dict)
            {
            }

            template<class type>
            void operator()(type const& object, char const* key, ...) // just forwarding to outer functions for reflection
            {
                using namespace dict;
                write(bd_, object, key);
            }

        private:
            py::dict& bd_;
        };

        //////////////////////////////////////////////////////////////////////////

        struct read_processor
        {
            explicit read_processor(py::dict const& dict, bool log_on_absence = true)
                : bd_(dict)
                , log_on_absence_(log_on_absence)
            {
            }

            template<class type>
            void operator()(type& object, char const* key, ...) // just forwarding to outer functions for reflection
            {
                using namespace dict;
                read(bd_, object, key, log_on_absence_);
            }

        private:
            py::dict const&   bd_;
            bool            log_on_absence_;
        };

    } // details 

    template<class type>
    void write(py::dict& dict, type const& object, const char* key)
    {
        py::object obj;
        write(obj, object);
        dict[key] = obj;
    }

    template<class type>
    void read(py::dict const& dict, type& object, const char* key, bool log_on_absence)
    {
        if (dict.has_key(key) && dict[key])
            read(dict[key], object, log_on_absence);
        else
        {
            if (log_on_absence)
                LogWarn("key \"" << key << "\" not found");
            //LogWarn("py::dict loading: \"" << key << "\" not found");
            return;
        }
    }


    template<class type>
    void write(py::object& pyobj, type const& object, type_traits::if_leaf_type<type>* = 0)
    {
        pyobj = py::object(object);
    }

    template<class type>
    void read(py::object const& pyobj, type & object, bool /*log_on_absence*/, type_traits::if_leaf_type<type>* = 0)
    {
        object = py::extract<type>(pyobj);
    }


    template<class type>
    void write(py::object& pyobj, type const& object, type_traits::if_not_leaf_n_container<type>* = 0)
    {
        py::dict dict;
        details::write_processor wp(dict);
        reflect(wp, object);
        pyobj = dict;
    }

    template<class type>
    void read(py::object const& pyobj, type& object, bool log_on_absence = true, type_traits::if_not_leaf_n_container<type>* = 0)
    {
        details::read_processor rp(py::extract<py::dict>(pyobj), log_on_absence);
        reflect(rp, object);
    }

}