/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once 

#include "application/panels/attr_panel.h"
#include "reflection/reflection_ext.h"
#include "attributes/attributes.h"

#include "common/no_deduce.h"
#include "common/font_descr.h"

#include "db_utils/refl/refl.h"

namespace prop_attr
{

namespace details 
{

template<class T, class F>
inline F value_getter(T & value)
{
    return (F)value;
}

template<class T, class F>
inline void value_setter(T & value, F new_value)
{
    value = (T)new_value;
}

template<class T>
inline app::attr::attr_variant variant_value_getter(T & value)
{
    return app::attr::attr_variant(value);
}

template<class T>
struct variant_set_visitor : boost::static_visitor < T >
{
    template<class F>
    T operator()(F const&  ) const { Assert(0); return T(); }
    T operator()(T const& i) const { return i; }
};

template<class T>
inline void variant_value_setter(T & value, app::attr::attr_variant const& new_value)
{
    value = boost::apply_visitor(variant_set_visitor<T>(), new_value);
}

template<class T, class F>
inline F opt_inner_getter(optional<T> & value, F const& def)
{
    return value ? F(*value) : def;
}

template<class T, class F>
inline void opt_inner_setter(optional<T> & value, F const& new_value)
{
    if (value)
        value = T(new_value);
}

template<class T>
inline bool opt_getter(optional<T> & value)
{
    return value ? true : false;
}

template<class T>
inline void opt_setter(optional<T> & value, bool new_value)
{
    if (new_value && !value)
        value = T();
    else if (!new_value)
        value.reset();
}


} // details

// forward
template<class T> struct numeric_editor_tag;
template<class editor_tag, typename editor_tag::data_type::value_type(*def_func)()> struct opt_editor_tag;

template<class type>
void fill_attr( app::attr::attr_group_ptr item, type& object, char const *name, bool read_only = false,
                typename std::enable_if< !app::attr::is_simple_type<type>::value >::type* = 0 );

template<class type, class editor, typename editor::data_type::value_type (*def_func)()>
void fill_attr(app::attr::attr_group_ptr item, optional<type>& object, char const *name, opt_editor_tag<editor, def_func> const& editor_tag);

template<class type, class editor_type>
void fill_attr(app::attr::attr_group_ptr item, type & object, char const *name, editor_type const& editor_tag);

template<class type>
void fill_attr(app::attr::attr_group_ptr item, type & object, char const *name, numeric_editor_tag<type> const& editor_tag,
                typename std::enable_if<std::is_arithmetic<type>::value>::type * = 0);

template<class type, class editor_t>
void fill_attr(app::attr::attr_group_ptr item, optional<type>& object, char const *name, editor_t const& editor_tag);

template<class type, class editor_t>
void fill_attr(app::attr::attr_group_ptr item, optional<type>& object, char const *name, editor_t const& editor_tag, 
               typename editor_t::data_type::value_type def);

template<class type>
void fill_attr( app::attr::attr_group_ptr item, type& object, char const *name, bool read_only = false,
                typename std::enable_if< app::attr::is_simple_type<type>::value &&
                                         app::attr::is_default_variant_editor<type>::value>::type* = 0 );

template<class T, class A>
void fill_attr( app::attr::attr_group_ptr item, vector<T, A>& vec, char const *name );

template <class K, class V, class C, class A>
void fill_attr( app::attr::attr_group_ptr item, map<K, V, C, A> &m, char const *name, typename std::enable_if< !std::is_arithmetic<K>::value && !std::is_same<K, std::string>::value >::type* = 0);

template <class K, class V, class C, class A>
void fill_attr( app::attr::attr_group_ptr item, map<K, V, C, A> &m, char const *name, typename std::enable_if< std::is_integral<K>::value || std::is_same<K, std::string>::value >::type* = 0);

template <class K, class V, class C, class A>
void fill_attr( app::attr::attr_group_ptr item, map<K, V, C, A> &m, char const *name, typename std::enable_if< std::is_floating_point<K>::value >::type* = 0);

template<class T, size_t S>
void fill_attr( app::attr::attr_group_ptr item, std::array<T, S> &array, char const *name);

template<class T, size_t S, class editor>
void fill_attr(app::attr::attr_group_ptr item, std::array<T, S> &array, char const *name, editor const& editor_tag);

template <class T>
void fill_attr( app::attr::attr_group_ptr item, optional<T>& opt, char const *name );

// Editors tags

template<size_t k>
struct uom_editor_tag
{
    typedef app::attr::uom_editor_data data_type;

    uom_editor_tag(optional<double> min, optional<double> max, optional<double> step = boost::none, bool read_only = false)
        : data_((ui_cfg::uom_kind)k, min, max, step, read_only)
    {}

    uom_editor_tag(bool read_only = false)
        : data_((ui_cfg::uom_kind)k, boost::none, boost::none, boost::none, read_only)
    {}

    data_type const& data() const { return data_; }

private:
    data_type data_;
};

template<>
struct uom_editor_tag<ui_cfg::UOM_PORTION>
{
    typedef app::attr::uom_editor_data data_type;

    uom_editor_tag(bool read_only = false)
        : data_(ui_cfg::UOM_PORTION, 0., 1., 0.1, read_only)
    {}
    uom_editor_tag(optional<double> min, optional<double> max, optional<double> step = boost::none, bool read_only = false)
        : data_(ui_cfg::UOM_PORTION, min, max, step, read_only)
    {}

    data_type const& data() const { return data_; }

private:
    data_type data_;
};

typedef  struct uom_editor_tag<ui_cfg::UOM_PORTION> uom_portion_tag;

template<size_t k>
struct time_editor_tag 
{
    typedef app::attr::time_editor_data data_type;

    time_editor_tag(bool read_only = false)
        : data_((ui_cfg::time_kind)k, read_only)
    {}

    data_type const& data() const { return data_; }

private:
    data_type data_;
};

struct date_time_editor_tag
{
    typedef app::attr::date_time_editor_data data_type;

    date_time_editor_tag() {}

    data_type const& data() const { return data_; }

private:
    data_type data_;
};

template<size_t k>
struct lat_lon_editor_tag
{
    typedef app::attr::lat_lon_editor_data data_type;
    
    lat_lon_editor_tag(bool read_only = false)
        : data_((ui_cfg::lat_lon_kind)k, read_only)
    {}

    data_type const& data() const { return data_; }

private:
    data_type data_;
};

struct enum_editor_tag
{
    typedef app::attr::enum_editor_data data_type;

    enum_editor_tag(std::initializer_list<const char*> const& value_names, bool read_only = false) 
        : value_names_(value_names) 
        , read_only_(read_only) 
    {}

    app::attr::enum_editor_data data() const
    {
        return app::attr::enum_editor_data(value_names_, read_only_);
    }

private:
    std::initializer_list<const char*> const& value_names_;
    bool read_only_;
};

struct flag_enum_editor_tag
{
    typedef app::attr::flag_enum_editor_data data_type;

    flag_enum_editor_tag(std::initializer_list<const char*> const& value_names, bool read_only = false)
        : value_names_(value_names)
        , read_only_(read_only)
    {}

    app::attr::flag_enum_editor_data data() const
    {
        return app::attr::flag_enum_editor_data(value_names_, read_only_);
    }

private:
    std::initializer_list<const char*> const& value_names_;
    bool read_only_;
};

template<class T>
struct numeric_editor_tag
{
    typedef app::attr::variant_editor_data data_type;

    numeric_editor_tag(T min, T max, optional<T> step = boost::none, optional<int> decimals = boost::none, bool read_only = false)
        : data_(read_only)
    {
        data_.add_attribute("minimum", min);
        data_.add_attribute("maximum", max);
        if (step)       
            data_.add_attribute("singleStep", *step);

        if (decimals)   
            data_.add_attribute("decimals", *decimals);
        else if (step)
        {
            int num_digits = (int)std::floor(log10(*step));
            data_.add_attribute("decimals", num_digits > 0 ? 0 : std::abs(num_digits));
        }
    }

    data_type const& data() const { return data_; }

    numeric_editor_tag(bool read_only = false)
        : data_(read_only)
    {}


private:
    data_type data_;
};

template<class T>
T opt_def_func() { return T(); }

template<class editor_tag, typename editor_tag::data_type::value_type (*def_func)() = &opt_def_func<editor_tag::data_type::value_type>>
struct opt_editor_tag
{
    typedef typename editor_tag::data_type::value_type inner_value_type;

    opt_editor_tag(editor_tag const& e, inner_value_type def = def_func())
        : next(e)
        , def(def)
    {}


    inner_value_type def;
    editor_tag next;
};


struct read_only_tag {};

typedef prop_attr::uom_editor_tag<ui_cfg::UOM_RANGE>            range_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_WIDTH>            width_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_RW_RANGE>         rw_range_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_RW_THICKNESS>     rw_thickness_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_TEMPERATURE>      temperature_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_GROUND_SPEED>     ground_speed_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_IAS>              ias_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_TAS>              tas_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_ROCD>             rocd_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_PORTION>          portion_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_ALTITUDE>         altitude_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_FL>               fl_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_ANGLE>            angle_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_WIND_SPEED>       wind_speed_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_PRESSURE>         pressure_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_FREQUENCY>        frequency_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_DENSITY>          density_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_AREA>             area_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_MASS>             mass_tag;
typedef prop_attr::uom_editor_tag<ui_cfg::UOM_ACCEL>            accel_tag;

//////////////////////////////////////////////////////////////////////////
// reflection processors
struct fill_processor
{
    explicit fill_processor(app::attr::attr_group_ptr item)
        : item(item)
    {
    }

    template<class type, class editor>
    void operator()(type& object, char const *name, editor const& editor_tag)
    {
        fill_attr(item, object, name, editor_tag);
    }

    template<class type>
    void operator()(type& object, char const *name, read_only_tag const &)
    {
        fill_attr(item, object, name, true); // just forwarding to outer functions
    }

    template<class type>
    void operator()(type& object, char const *name)
    {
        fill_attr(item, object, name); // just forwarding to outer functions
    }

    template<class type>
    void operator()(type& object, char const *name, db_utils::db_refl::flat_tag)
    {
        fill_attr(item, object, name); // just forwarding to outer functions
    }

    template<class type>
    void operator()(type& object, char const *name, db_utils::db_refl::json_tag)
    {
        fill_attr(item, object, name); // just forwarding to outer functions
    }

    template<class type>
    void operator()(type& /*object*/, char const* /*name*/, refl::just_serialization)
    {
        // do nothing
    }

    template<class type>
    void operator()(type& object, char const* name, double min_value, double max_value, double single_step)
    {
        item->add(&object, name, min_value, max_value, single_step);
    }

    app::attr::attr_group_ptr item;
};

template<class type>
void fill_attr( app::attr::attr_group_ptr item, type& object, char const *name, bool read_only,
                typename std::enable_if< !app::attr::is_simple_type<type>::value >::type*)
{
    app::attr::attr_group_ptr i = item->create_group(name, read_only);
    fill_processor proc(i);
    reflect(proc, object);

    item->add(i);
}

template<class type, class editor_type>
void fill_attr(app::attr::attr_group_ptr item, type & object, char const *name, editor_type const& editor_tag)
{
    using namespace app::attr;

    typedef typename editor_type::data_type data_type;
    typedef typename data_type::value_type value_type;

    app::attr::attr_item_ptr i = item->create_attr_item((editor_kind_t)data_type::editor_kind, name);
    dynamic_cast<attr_item_data<data_type> *>(i.get())->set_data(editor_tag.data());
    auto g = boost::bind(details::value_getter<type, value_type>, boost::ref(object));
    auto s = boost::bind(details::value_setter<type, value_type>, boost::ref(object), _1);
    dynamic_cast<attr_item_callbacks<value_type> *>(i.get())->set_callbacks(g, s);
    item->add(i);
}

template<class type>
void fill_attr(app::attr::attr_group_ptr item, type & object, char const *name, numeric_editor_tag<type> const& editor_tag,
    typename std::enable_if<std::is_arithmetic<type>::value>::type *)
{
    using namespace app::attr;

    app::attr::attr_item_ptr i = item->create_variant_item(name, object);
    auto g = boost::bind(details::variant_value_getter<type>, boost::ref(object));
    auto s = boost::bind(details::variant_value_setter<type>, boost::ref(object), _1);
    dynamic_cast<attr_item_callbacks<attr_variant> *>(i.get())->set_callbacks(g, s);
    dynamic_cast<attr_item_data<variant_editor_data> *>(i.get())->set_data(editor_tag.data());
    item->add(i);
}

template<class type, class editor_t>
void fill_attr(app::attr::attr_group_ptr item, optional<type>& object, char const *name, editor_t const& editor_tag)
{
    typedef editor_t::data_type data_type;
    typedef data_type::value_type value_type;

    fill_attr(item, object, name, editor_tag, value_type());
}

template<class type, class editor, typename editor::data_type::value_type (*def_func)()>
void fill_attr(app::attr::attr_group_ptr item, optional<type>& object, char const *name, opt_editor_tag<editor, def_func> const& editor_tag)
{
    fill_attr(item, object, name, editor_tag.next, editor_tag.def);
}


template<class type, class editor_t>
void fill_attr( app::attr::attr_group_ptr item, optional<type>& object, char const *name, editor_t const& editor_tag, 
                typename editor_t::data_type::value_type def)
{
    using namespace app::attr;

    typedef editor_t::data_type data_type;
    typedef data_type::value_type value_type;

    attr_item_ptr i = item->create_opt_item(name);
    auto g = boost::bind(details::opt_getter<type>, boost::ref(object));
    auto s = boost::bind(details::opt_setter<type>, boost::ref(object), _1);
    attr_opt_item_ptr(i)->set_callbacks(g, s);

    attr_item_ptr inner = attr_opt_item_ptr(i)->create_attr_item((editor_kind_t)data_type::editor_kind, name);
    dynamic_cast<attr_item_data<data_type> *>(inner.get())->set_data(editor_tag.data());
    auto og = boost::bind(details::opt_inner_getter<type, value_type>, boost::ref(object), def);
    auto os = boost::bind(details::opt_inner_setter<type, value_type>, boost::ref(object), _1);
    dynamic_cast<attr_item_callbacks<value_type> *>(inner.get())->set_callbacks(og, os);

    attr_opt_item_ptr(i)->set_inner(inner);
    item->add(i);
}


template<class type>
void fill_attr( app::attr::attr_group_ptr item, type& object, char const *name, bool read_only,
                typename std::enable_if< app::attr::is_simple_type<type>::value &&
                                         app::attr::is_default_variant_editor<type>::value>::type*)
{
    using namespace app::attr;

    app::attr::attr_item_ptr i = item->create_variant_item(name, object);
    auto g = boost::bind(details::variant_value_getter<type>, boost::ref(object));
    auto s = boost::bind(details::variant_value_setter<type>, boost::ref(object), _1);
    dynamic_cast<attr_item_callbacks<attr_variant> *>(i.get())->set_callbacks(g, s);
    dynamic_cast<attr_item_data<variant_editor_data> *>(i.get())->set_data(variant_editor_data(read_only));
    item->add(i);
}

// STL containers
template<class T, class A>
void fill_attr( app::attr::attr_group_ptr item, vector<T, A> &vec, char const *name)
{
    app::attr::attr_group_ptr vec_item = item->create_group(name);
    for (size_t i = 0; i < vec.size(); ++i)
        fill_attr(vec_item, vec[i], lexical_cast<string>(i).c_str());
    item->add(vec_item);
}

template <class K, class V, class C, class A>
void fill_attr( app::attr::attr_group_ptr item, map<K, V, C, A> &m, char const *name, typename std::enable_if< !std::is_arithmetic<K>::value && !std::is_same<K, std::string>::value >::type*)
{
    app::attr::attr_group_ptr map_item = item->create_group(name);
    for (auto it = m.begin(); it != m.end(); ++it)
    {
        fill_attr(map_item, const_cast<K&>(it->first), "key"  );
        fill_attr(map_item, it->second               , "value");
    }
    item->add(map_item);
}

template <class K, class V, class C, class A>
void fill_attr( app::attr::attr_group_ptr item, map<K, V, C, A> &m, char const *name, typename std::enable_if< std::is_integral<K>::value || std::is_same<K, std::string>::value >::type*)
{
    app::attr::attr_group_ptr map_item = item->create_group(name);
    for (auto it = m.begin(); it != m.end(); ++it)
    {
        fill_attr(map_item, it->second, lexical_cast<string>(it->first).c_str());
    }
    item->add(map_item);
}

template <class K, class V, class C, class A>
void fill_attr( app::attr::attr_group_ptr item, map<K, V, C, A> &m, char const *name, typename std::enable_if< std::is_floating_point<K>::value >::type*)
{
    app::attr::attr_group_ptr map_item = item->create_group(name);
    for (auto it = m.begin(); it != m.end(); ++it)
    {
        std::ostringstream oss;
        oss << std::fixed << std::setprecision(1) << it->first;
        fill_attr(map_item, it->second, oss.str().c_str());
    }
    item->add(map_item);
}

template<class T, size_t S>
void fill_attr( app::attr::attr_group_ptr item, std::array<T, S> &array, char const *name)
{
    app::attr::attr_group_ptr array_item = item->create_group(name);
    for (size_t i = 0; i < S; ++i)
        fill_attr(array_item, array[i], lexical_cast<string>(i).c_str());
    item->add(array_item);
}

template<class T, size_t S, class editor>
void fill_attr(app::attr::attr_group_ptr item, std::array<T, S> &array, char const *name, editor const& editor_tag)
{
    app::attr::attr_group_ptr array_item = item->create_group(name);
    for (size_t i = 0; i < S; ++i)
        fill_attr(array_item, array[i], lexical_cast<string>(i).c_str(), editor_tag);
    item->add(array_item);
}


/////////////////////////////////////////////////////////////////////////
// short way for attribute filling
template<typename type>
void fill_widget(type& object, ::app::attr::attr_group_ptr item)
{
    fill_processor proc(item);
    reflect(proc, object);
}

template<typename type>
void fill_widget(type& object, ::app::attr_panel *panel, string const& name = string())
{
    fill_widget(object, panel->add_item(!name.empty() ? name : typeid(type).name()));
}


} // prop_attr

namespace pa = prop_attr;