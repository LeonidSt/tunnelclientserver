/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once 
#include "reflection_aux.h"

template<typename base_processor_type>
struct single_entry_processor
{
    single_entry_processor(base_processor_type &base_proc)
        : base_proc_(base_proc)
    {}

    template<typename T, typename... Args>
    void operator()(T &&entry, T &&entry2, char const *name, Args&&... args)
    {
        (void)entry2;
        Assert(&entry == &entry2);
        base_proc_(std::forward<T>(entry), name, std::forward<Args>(args)...);
    }

private:
    base_processor_type &base_proc_;
};

template<typename Proc, typename T>
void reflect(Proc &proc, T const &object)
{
    single_entry_processor<Proc> proc_wrapper(proc);
    reflect2(proc_wrapper, object, object);
}

#define REFL_STRUCT_BODY(...)                           \
void reflect2(processor& proc, __VA_ARGS__ const& lhs, __VA_ARGS__ const& rhs)\
{                                                       \
    typedef __VA_ARGS__ type;                           \
    type& lobj = const_cast<type&>(lhs);/*small hack*/  \
    type& robj = const_cast<type&>(rhs);/*small hack*/  \
                                                        \
    (void) proc; /*avoiding warning*/                   \
    (void) lobj; /*avoiding warning*/                   \
    (void) robj; /*avoiding warning*/                   


// for using outside structure or class
#define REFL_STRUCT(type)                               \
template<class processor>                               \
    REFL_STRUCT_BODY(type)

// for using inside structure or class - mostly for templates
#define REFL_INNER(type)                        \
template<class processor>                       \
    friend REFL_STRUCT_BODY(type)

#define REFL_ENTRY_NAMED(entry, name)           \
    proc(lobj.entry, robj.entry, name);

#define REFL_ENTRY(entry)                       \
    proc(lobj.entry, robj.entry, #entry);    

#define REFL_CHAIN(base)                        \
    reflect2(proc, (base&)lobj, (base&)robj);

#define REFL_AS_TYPE_NAMED(entry, type, name)   \
    refl::process_as_type<type>                 \
        (proc, lobj.entry, robj.entry, name);

#define REFL_AS_TYPE(entry, type)               \
    refl::process_as_type<type>                 \
    (proc, lobj.entry, robj.entry, #entry);

#define REFL_END()                              \
}



//////////////////////////////////////////////////////////////////////////
// simple sample: 

// struct group
// {
//     string name;
// };
// 
// REFL_STRUCT(group)
//     REFL_ENTRY(name)
// REFL_END   ()
// 
// struct user 
// {
//     string          name;
//     vector<group>   groups;
// 
//     REFL_INNER(person)
//         REFL_ENTRY(name)
//         REFL_ENTRY(groups)
//     REFL_END  ()
// };
// 
