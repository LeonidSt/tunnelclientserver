/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once

namespace locks
{
struct bool_lock
{
    bool_lock(bool &val, bool no_recursive = true)
        : val_(val)
    {
        if (no_recursive)
            Assert(!val);
        val_ = true;
    }
    ~bool_lock()
    {
        val_ = false;
    }

    private:
        bool& val_;
};
} // namespace locks
