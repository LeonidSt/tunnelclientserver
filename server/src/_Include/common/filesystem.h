/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once


inline void remove_recursively(boost::filesystem::path const& path, bool log_errors = false)
{
    namespace fs = boost::filesystem;

    if (fs::exists(path))
    {
        boost::system::error_code ec;

        std::queue<fs::path>  visit;
        visit.push(path);

        while (!visit.empty())
        {
            fs::path cur_path = std::move(visit.front());
            visit.pop();

            fs::remove_all(cur_path, ec);
            if (ec)
            {
                if (log_errors)
                    LogError("Can't remove " << cur_path.string() << ": " << ec.message());

                if (fs::is_directory(cur_path))
                {
                    typedef fs::directory_iterator dit;
                    for (dit dirs(cur_path); dirs != dit(); ++dirs)
                        visit.push(dirs->path());
                }
            }
        }
    }
}
