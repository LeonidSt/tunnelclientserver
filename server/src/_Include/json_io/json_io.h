/*
    * Copyright (C) 2015 SimLabs LLC - All rights reserved.
    * Unauthorized copying of this file or its part is strictly prohibited.
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once

#include "reflection/proc/dict_refl.h"

namespace json_io
{

dict_t stream_to_dict(std::istream &json_stream);
void dict_to_stream(std::ostream &stream, dict_t const &d, bool pretty);

template<typename T>
void stream_to_data(std::istream &stream, T &data)
{
    dict_t d = stream_to_dict(stream);
    {
        time_counter tc;
        dict::read(d, data);
    }
}

template<typename T>
void data_to_stream(std::ostream &stream, T const &data, bool pretty)
{
    dict_t d(false);
    dict::write(d, data);
    dict_to_stream(stream, d, pretty);
}

template<typename T>
void string_to_data(std::string const &json, T &data)
{
    std::istringstream ss(json);
    stream_to_data(ss, data);
}

template<typename T>
string data_to_string(T const &data, bool pretty)
{
    std::ostringstream ss;
    data_to_stream(ss, data, pretty);
    return ss.str();
}


} // namespace json_io
