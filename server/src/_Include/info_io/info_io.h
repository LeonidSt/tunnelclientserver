/* 
    * Copyright (C) 2014 SimLabs LLC - All rights reserved. 
    * Unauthorized copying of this file or its part is strictly prohibited. 
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once 

#include "serialization/dict_fwd.h"
#include "boost/filesystem.hpp"

namespace info_io
{

namespace fs = boost::filesystem;

// Interface

void write(dict::dict_t const& dict, std::string& str);
void write(dict::dict_t const& dict, fs::path const& path);

void read(dict::dict_t& dict, std::string const& str, fs::path const& file_folder);
void read(dict::dict_t& dict, fs::path    const& path);
void read(dict::dict_t& dict, std::istream& is, fs::path const& file_folder);

} // info_io
