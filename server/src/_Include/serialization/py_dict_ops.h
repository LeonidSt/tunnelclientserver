/*
    * Copyright (C) 2015 SimLabs LLC - All rights reserved.
    * Unauthorized copying of this file or its part is strictly prohibited.
    * For any information on this file or its part please contact: support@sim-labs.com
*/

#pragma once

namespace script
{

//////////////////////////////////////////////////////////////////////////
// optional<T>

template<class T>
void write(py::object& obj, boost::optional<T> const& opt)
{
    if (opt)
        write(obj, *opt);
}

template<class T>
void read(py::object const& obj, boost::optional<T>& opt, bool log_on_absence = true)
{
    if (obj)
    {
        opt.reset(T());
        read(obj, *opt, log_on_absence);
    }
    else
        opt.reset();
}


}